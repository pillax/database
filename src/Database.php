<?php
namespace vendor\pillax\database\src;

use app\config\Config;
use vendor\pillax\Arr;

class Database extends \PDO {

    private static $instances = [];
    private $binderIncrementor = 0;

    /**
     * Multitone
     *
     * @param string $connectionName
     * @return database
     */
    public static function init($connectionName) : database {
        if( ! isset(self::$instances[$connectionName])) {
            $cnf = Config::$databases[$connectionName];
            self::$instances[$connectionName] = new self(
                'mysql:host=' . $cnf['host'] . ';dbname=' . $cnf['name'],
                $cnf['user'],
                $cnf['pass'],
                [
                    \PDO::MYSQL_ATTR_INIT_COMMAND   => "SET NAMES 'utf8'",
                    \PDO::ATTR_ERRMODE              => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE   => \PDO::FETCH_ASSOC,
                ]
            );
        }
        return self::$instances[$connectionName];
    }

    /**
     * Get all rows from query result
     * Example: 'SELECT id, title FROM `news`'
     * [
     *  [id => 1, title => 'aaa'],
     *  [id => 2, title => 'bbb'],
     *  [id => 3, title => 'ccc'],
     * ]
     *
     * @param string $sql
     * @param array $binders
     * @param int $fetchStyle PDO::FETCH_ASSOC|...
     * @return array
     */
    public function getRows($sql, array $binders=[], $fetchStyle=null) : array {
        $stm = $this->prepare($sql);
        $stm->execute($binders);
        return $stm->fetchAll($fetchStyle);
    }

    /**
     * Get first row from query result
     * Example: 'SELECT id, title FROM `news`'
     * [
     *  id => 1,
     *  title => 'aaa'
     * ]
     *
     * @param string $sql
     * @param array $binders
     * @return array
     */
    public function getRow($sql, array $binders=[]) : array {
        $stm = $this->prepare($sql);
        $stm->execute($binders);
        return $stm->fetch();
    }

    /**
     * Get all rows as columns
     * Example: 'SELECT id, title FROM `news`'
     * [
     *  id => [1,2,3],
     *  title => ['aaa', 'bbb', 'ccc'],
     * ]
     *
     * @param string $sql
     * @param array $binders
     * @return array
     */
    public function getCols($sql, array $binders=[]) : array {
        $result = $this->getRows($sql, $binders);
        $out = [];
        if(isset($result[0])) {
            $cols = array_keys($result[0]);
            foreach($cols AS $col) {
                $out[$col] = array_column($result, $col);
            }
        }
        return $out;
    }

    /**
     * Get first column
     * Example: 'SELECT id, title FROM `news`'
     * [1,2,3]
     *
     * @param string $sql
     * @param array $binders
     * @return array
     */
    public function getCol($sql, array $binders=[]) : array {
        $result = $this->getRows($sql, $binders, \PDO::FETCH_NUM);
        return $result ? array_column($result, 0) : [];
    }

    /**
     * Get firs cell from the first row from query result
     * Example: 'SELECT id, title FROM `news`'
     * 1
     *
     * @param string $sql
     * @param array $binders
     * @return string
     */
    public function getCell($sql, array $binders=[]) {
        $stm = $this->prepare($sql);
        $stm->execute($binders);
        return $stm->fetchColumn();
    }

    /**
     * Get firs cell from the first row from query result
     * Example: 'SELECT id, title FROM `news`'
     * [
     *   1 => 'aaa',
     *   2 => 'bbb',
     *   3 => 'ccc',
     * ]
     *
     * @param string $sql
     * @param array $binders
     * @return array
     */
    public function getPairs($sql, array $binders=[]) {
        return $this->getRows($sql, $binders, \PDO::FETCH_KEY_PAIR);
    }

    /**
     * Insert row(s) in table
     * TODO: update on duplicated keys
     *
     * Examples
     * - normal insert:
     *  $db->insert('news', ['title', 'authorId'], ['aaa', 1]);
     *
     * - bulk insert
     *  $db->insert('news', ['title', 'authorId'], [
     *      ['aaa', 1],
     *      ['bbb', 2],
     *      ['ccc', 2]
     *  ]);
     *
     * @param string $table
     * @param array $fields
     * @param array $values Can be 2-dimensional for bulk insert
     * @return bool always true
     */
    public function insert($table, array $fields, array $values) : bool {
        $rows = $values;
        if ( ! Arr::isMulti($rows)) {
            // make $values 2-dimensional array
            $rows = [$rows];
        }

        $binders = [];
        $sqlAppendixes = [];
        foreach ($rows AS $row) {
            $rowBinders = [];
            foreach ($row AS $value) {
                $binderName = $this->getBinderName();
                $rowBinders[$binderName] = $value;
            }
            $binders = array_merge($binders, $rowBinders);
            $sqlAppendixes[] = '(' . implode(',', array_keys($rowBinders)) . ')';
        }

        $sql = '
        INSERT 
        INTO ' . $table . ' (' . implode(',', $fields) . ') 
        VALUES ' . implode(',', $sqlAppendixes);
        $this->prepare($sql)->execute($binders);

        return true;
    }

    /**
     * @param $table
     * @param array $fields
     * @param array $values
     * @return bool
     */
    public function update($table, array $data, $where, $whereBinders) : bool {
        $binders = [];
        $sqlAppendixes = [];

        foreach ($data AS $field => $value) {
            $binderName = $this->getBinderName();
            $binders[$binderName] = $value;
            $sqlAppendixes[] = $field . ' = ' . $binderName;
        }

        $sql = '
        UPDATE ' . $table . ' 
        SET ' . implode(',', $fields) . ' 
        VALUES ' . implode(',', $sqlAppendixes);
        $this->prepare($sql)->execute($binders);

        return true;
    }
    
    
    
    
    
    
    /**
     * Get safe binder name for the current request
     *
     * @param string $name
     * @return string
     */
    public function getBinderName($name=':binder') : string {
        return $name . ++$this->binderIncrementor;
    }
    
    
    
    
    // Query builder
    // update
    // Qbuilder
    // cache
    // transactions
// https://bitbucket.org/alexamiryan/stingle/src/240c9b34cc50b19da5fc78fabc2adef8c772d70e/packages/Db/QueryBuilder/Managers/QueryBuilder.class.php?at=master&fileviewer=file-view-default

}