<?php
namespace vendor\pillax\database\src;
use app\config\Config;
use vendor\pillax\simpleException\InvalidArgumentException\src;

/**
 * Class Query Builder
 * @package vendor\pillax\database
 */
class QueryBuilder {

    private static $instance;
    private $db;
    private $table;

    private $where = [];
    private $binders = [];

    const SELECT_METHOD_GET_ROWS    = 'getRows';
    const SELECT_METHOD_GET_ROW     = 'getRow';
    const SELECT_METHOD_GET_COLS    = 'getCols';
    const SELECT_METHOD_GET_COL     = 'getCol';
    const SELECT_METHOD_GET_CELL    = 'getCell';
    const SELECT_METHOD_GET_PAIRS   = 'getPairs';

    private $selectMethods = [
        self::SELECT_METHOD_GET_ROWS,
        self::SELECT_METHOD_GET_ROW,
        self::SELECT_METHOD_GET_COLS,
        self::SELECT_METHOD_GET_COL,
        self::SELECT_METHOD_GET_CELL,
        self::SELECT_METHOD_GET_PAIRS,
    ];


    public static function table(string $table, ?Database $db=null) : self {
        if( ! self::$instance) {
            self::$instance = new self($table, $db);
        }
        return self::$instance;
    }

    private function __construct(string $table, ?Database $db=null) {
        $this->table = $table;
        if( ! $db) {
            $db = Database::init(Config::DB_DEFAULT);
        }
        $this->db = $db;
    }




    public function select(array $fields=[], string $fetchMethod='getRows') {
        $sql = ' SELECT ';
        $sql .= $fields ? implode(', ', $fields) : '*';
        $sql .= ' FROM ' . $this->table;
        $sql .= ' WHERE ' . implode(' ', $this->where);

        if( ! in_array($fetchMethod, $this->selectMethods)) {
            throw new InvalidArgumentException('asd', [
                'url'    => 123,
            ]);
        }
        return $this->db->getRows($sql, $this->binders);
    }




    public function plainWhere($sql, $binders) : self {
        $this->binders += $binders;
        $this->where[] = $sql;
        return $this;
    }

    public function andWhere($field, $comparison='', $value=null) : self {
        $this->where('AND', $field, $comparison, $value);
        return $this;
    }

    public function orWhere($field, $comparison='', $value=null) : self {
        $this->where('OR', $field, $comparison, $value);
        return $this;
    }

    private function where($operator, $field, $comparison='', $value=null) {
        $lastElement = end($this->where);

        // Suppress operator when:
        //  - this is a first condition in where claues
        //  - previous condition begins with (
        if( ! $lastElement || strrpos((string) $lastElement, '(', 3) !== false) {
            $operator = '';
        }

        if($field instanceof \Closure) {
            $this->where[] = $operator . ' ( ';
            $field($this);
            $this->where[] = ' ) ';
        } else {
            $binder = $this->db->getBinderName();
            $this->binders[$binder] = $value;
            $this->where[] = $operator . ' ' . $field . ' ' . $comparison . ' ' . $binder;
        }
    }



    // plainWhere

    public function exec() {

        $sql = ' SELECT ';
        $sql .= $this->select ? implode(', ', $this->select) : '*';
        $sql .= ' FROM ' . $this->table;
        $sql .= ' WHERE ' . implode(' ', $this->where);
        echo $sql;
        dd($this->db->getRows($sql, $this->binders));

    }



}